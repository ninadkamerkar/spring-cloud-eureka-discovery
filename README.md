1. Build all the 3 projects

2. Start the discovery-server using DiscoveryServerApplication.java. 
   This will start the EUREKA server on port 8761.
   Hit http://localhost:8761/ url in browser which should display the eureka home page.
   
3. Start the hello-service project using HelloServiceApplication.java.
   This will start the hello service on random port as server.port=0.
   Hit http://localhost:<port>/hello/<Name> to test whether the hello service is started or not
   Replace <port> with the exact port and <name> with some text
   E.g. http://localhost:8080/hello/Ninad
   Once started, check the EUREKA dashboard to see whether hello service is registered.
   
4. Repeat step 3 to run another instance of hello-service. Check EUREKA dashboard to confirm same.

5. Start the hello-app using HelloApplication.java. 
   This will start the eureka client application on port 9999.
   Hit http://localhost:9999/greeting/loadBalanced/<name> to test whether hello app is able to discover hello-service using discovery.
   If you hit above url multiple time, you will see the message will change. Request will get load balanced using ribbon.
   
   Hit http://localhost:9999/greeting/normal/<name> to see how normal rest url works without eureka.